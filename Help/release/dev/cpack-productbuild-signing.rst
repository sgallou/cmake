cpack-productbuild-signing
--------------------------

* The :module:`CPackProductBuild` module gained options to sign packages.
  See the variables :variable:`CPACK_PRODUCTBUILD_IDENTITY_NAME`,
  :variable:`CPACK_PRODUCTBUILD_KEYCHAIN_PATH`,
  :variable:`CPACK_PKGBUILD_IDENTITY_NAME`, and
  :variable:`CPACK_PKGBUILD_KEYCHAIN_PATH`.
